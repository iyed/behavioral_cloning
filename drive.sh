#!/usr/bin/env bash

nvdocker run -it -p 4567:4567 -v /etc/protocols:/etc/protocols -v /home/iyed/carnd/CarND-Behavioral-Cloning-P3:/src -w /src tensorflow-gpu-py3 python3 drive.py "$1" "$2" 
