#**Behavioral Cloning Project**
---

####1. Contents:

* model.py containing the script to create and train the model
* drive.py the udacity provided file for driving the car in autonomous mode
* model02-final.h5 containing a trained convolution neural network 
* a Dockerfile
* train.sh and drive.sh wrapper bash scripts than run the python files in a docker container.
* the current README.md file

Please note that the container must be run with nvidia-docker not vanilla docker.


####2. Train the model

To train the model open a terminal and type:
```sh
./train.sh data_folder mode.h5
```
This repo contains a Dockerfile used to train the network in a docker container
configured with tensorflow with gpu enabled and all the required python packages.

####3. The model

The model.py file contains the code for training and saving the convolution neural network. The file shows the pipeline I used for training and validating the model, and it contains comments to explain how the code works. Keras 2.0 was used to code this model.

###Model Architecture
---

####1. An appropriate model architecture has been employed

I used the NVIDIA architecture for this project as referred to in the course and described [here.](https://devblogs.nvidia.com/parallelforall/deep-learning-self-driving-cars/)
You can find the architeture of the network in the model.py file.

####2. Attempts to reduce overfitting in the model

To prevent overfitting I added dropout layers with 0.2 dropout probability.
The error of prediction of the network was consistenly lower than on the train set. That showed
that the network is not overfitting and is able to generalize.

####3. Model parameter tuning
Hyper parameters used:

* epochs: 20
* Learning rate: 1-e03 
* batch size: 16 

####4. Appropriate training data

Training data was chosen to keep the vehicle driving on the road. I used a combination of center lane driving, recovering from the left and right sides of the road.
I made sure I spent more time driving on the centrer of the road than time driving away from the shoulders. I also stopped recording when I drove towards the shoulders
and resumed recording just before steering back to the center. For details aout the data set see the next section.


###Model Architecture and Training Strategy
---

####1. Solution Design Approach

I used the LeNet architecture first but could not get the car to drive automously. One reason for that may be the size of the input images and the number
of samples needed. LeNet was not able to capture the complexity of the input. Both training and validation error values were high (underfitting)
I switched to the NVIDIA architecture and preprocessed the input images as required by that architecture in the batch generator.
Choosing a powerful architecture was not enough to obtain the required behavior. More important is the input data. It's a
behavioral cloning after all. The cloned behavior can only be as good as the input data.

I logged driving while the car was in the center of the track and I steered to both left and right shoulders and back
to the center again to have enough samples of the car getting back to the center after it drifts off towards the shoulders.
I made sure I had more driving time with the car right in the center though: the normal behavior. But that was not enough and I had to
use the left and right images captured by the simulator because the car kept going off road. There is also a section of the track
that is particulary tricky: the one just after the brige. In order to avoid going off the track there, I took several time that turn
and kept the car as close as possible to the interior of the curve.

Below are examples of samples of center, left and right steering:

![](./examples/left_steering.jpg)
*left steering: -0.3157895*

![](./examples/right_steering.jpg)
*right steering: 0.3759399*

![](./examples/center.jpg)
*center: 0.0*


####2. Final Model Architecture

The final model implements the NVIDIA architecure plus the dropout layers between the convolutional layers.
Below is a visual of the model:

![model](./model.png)


####3. Creation of the Training Set & Training Process

Training data was chosen to keep the vehicle driving on the road. I used a combination of central lane driving, recovering from the left and right sides of the road.
To augment the data set, I also flipped images and angles to help the model generalize more and
to avoid the bias towards turning constantly to the left.
The input data set contains 15987 images plus every center image flipped.
I also applied histogram equalization on the input images.
I added a layer to normalize the pixels and mean center them.
Training the network was performed using batches of 64 images at a time (16x(3 + 1)).
below is an histogram showing the distribution of the input data set with respect to the steering angle:

![histogram](./hist.png)


As the histogram shows, the majority of the samples are coming from central lane driving. There is much less
images captured from the car steering left and even less images from the car steering right.
The unbalance between the images of right steering and left steering is due to driving the car
counter clockwise around the track. There also much more left curves than right curves.

