import sys
import os
import csv
import json
import random
import numpy as np
import cv2
from keras.models import Sequential
from keras.layers import Flatten, Dense, Lambda, Dropout
from keras.layers.convolutional import Cropping2D, Conv2D
from keras.layers.pooling import MaxPooling2D
from keras.utils import plot_model
import keras.backend as backend
from skimage import exposure
from sklearn.utils import shuffle
from skimage.io import imsave
from sklearn.model_selection import train_test_split
from keras.optimizers import Adam

datafolder = sys.argv[1]
modelname = sys.argv[2]
print('saving model to {}.'.format(modelname))

lines = []
with open('{}/driving_log.csv'.format(datafolder)) as csvfile:
    reader = csv.reader(csvfile)
    for line in reader:
        lines.append(line)

# remove the first line containing column names of the csv file
lines.pop(0)

# create train and validation sample sets
train_samples, validation_samples = train_test_split(lines, test_size=0.2)


def enhance_img(arr):
    arr = exposure.equalize_adapthist(arr)
    return np.array(arr*255, dtype=np.uint8)

# a generator to create batches
# batches are of lines of the csv file
# but the generator create a batch of 4xbatchsize
# containing images and associated steering angles
def gen_batch(samples, batchsize=16):
    num_samples = len(samples)
    while True:
        shuffle(samples)
        for offset in range(0, num_samples, batchsize):
            batch_samples = samples[offset:offset+batchsize]
            images = []
            steer = []
            for sample in batch_samples:
                path = '{}/IMG/{}'.format(datafolder, sample[0].split('/')[-1])
                img = cv2.imread(path) 
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                img = img[60:140, 0:320]  # crop the image
                img = cv2.resize(img, (200, 66), interpolation=cv2.INTER_AREA)
                enhance = random.choice([1, 2, 3])
                if enhance == 1:
                    images.append(enhance_img(img))
                else:
                    images.append(img)
                steer.append(float(sample[3]))
                # flip the image
                flipped = cv2.flip(img, 1)
                enhance = random.choice([1, 2])
                if enhance == 1:
                    images.append(enhance_img(flipped))
                else:
                    images.append(flipped)
                steer.append(-1.0 * float(sample[3]))

                path = '{}/IMG/{}'.format(datafolder, sample[1].split('/')[-1])
                img = cv2.imread(path)
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                img = img[60:140, 0:320]  # crop the image
                img = cv2.resize(img, (200, 66), interpolation=cv2.INTER_AREA)
                enhance = random.choice([1, 2])
                if enhance == 1:
                    images.append(enhance_img(img))
                else:
                    images.append(img)
                steer.append(float(sample[4]) + 0.2)

                path = '{}/IMG/{}'.format(datafolder, sample[2].split('/')[-1])
                img = cv2.imread(path)
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                img = img[60:140, 0:320]  # crop the image
                img = cv2.resize(img, (200, 66), interpolation=cv2.INTER_AREA)
                enhance = random.choice([1, 2])
                if enhance == 1:
                    images.append(enhance_img(img))
                else:
                    images.append(img)
                steer.append(float(sample[5]) - 0.2)

            X_train = np.array(images)
            y_train = np.array(steer)  
            yield shuffle(X_train, y_train)


# Hyper parameters
epochs = 20
batchsize = 16
learning_rate = 1e-03

train_gen = gen_batch(train_samples, batchsize)
valid_gen = gen_batch(validation_samples, batchsize)

# img = next(train_gen)[0][0]
# print(img.shape)
# imsave('./sample.jpg', img)
# exit()

model = Sequential()
model.add(Lambda(lambda x: x / 127.5 - 1.0, input_shape=(66, 200, 3)))
# model.add(Cropping2D(((70, 25), (0, 0))))

# Convolutions with relu activations
conv2d = Conv2D(24, (5, 5), strides=2, activation='elu')
conv2d_2 = Conv2D(36, (5, 5), strides=2, activation='elu')
conv2d_3 = Conv2D(48, (5, 5), strides=2, activation='elu')
conv2d_4 = Conv2D(64, (3, 3), activation='elu')
conv2d_5 = Conv2D(64, (3, 3), activation='elu')

model.add(conv2d)
model.add(Dropout(.2))
model.add(conv2d_2)
model.add(Dropout(.2))
model.add(conv2d_3)
# model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(.2))
model.add(conv2d_4)
model.add(Dropout(.2))
# model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(conv2d_5)
model.add(Flatten())

# Dense layers
model.add(Dense(100, activation='elu'))
model.add(Dense(50, activation='elu'))
model.add(Dense(10, activation='elu'))
model.add(Dense(1))

model.compile(loss='mse', optimizer=Adam(lr=learning_rate))
plot_model(model, to_file='model.png')
model.summary()
model.fit_generator(train_gen, steps_per_epoch=len(train_samples)/batchsize, validation_data=valid_gen, validation_steps=len(validation_samples)/batchsize, epochs=epochs)
model.save(modelname)

exit()

