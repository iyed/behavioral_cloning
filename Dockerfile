FROM gcr.io/tensorflow/tensorflow:latest-devel-gpu-py3

RUN apt-get update && apt-get install -y graphviz ffmpeg

RUN pip3 install keras pillow opencv-python h5py \
	python-socketio flask eventlet graphviz pydot-ng \
	scikit-image moviepy


